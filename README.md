# Mutation Tests Demo
Using mutation testing with a Java/Spring app for better test quality

---

## Installation

### Setup

- Clone and `git checkout v1`

### Usage

#### Seeing it work

##### Startup
```
./gradlew clean bootrun
```

##### Getting a good response

* [http://localhost:8080/randomuuid/3]()
* `curl -v http://localhost:8080/randomuuid/3`

Expected sample good response with three UUIDs from cURL:
```
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> GET /randomuuid/3 HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> 
< HTTP/1.1 200 
< Content-Type: application/json;charset=UTF-8
< Transfer-Encoding: chunked
< Date: Tue, 28 May 2019 17:36:02 GMT
< 
* Connection #0 to host localhost left intact
["79371c7b-158a-433f-b532-e11c7520669f","8e31ed9f-f733-4ceb-bb36-06972277dcf3","ed28120e-a54a-4b2a-b025-c4f984d27845"]

```

##### Getting a error response

* [http://localhost:8080/randomuuid/unknown]()
* `curl -v http://localhost:8080/randomuuid/unknown`

Expected sample error response from cURL:
```
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> GET /randomuuid/unknown HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.54.0
> Accept: */*
> 
< HTTP/1.1 400 
< Content-Type: application/json;charset=UTF-8
< Transfer-Encoding: chunked
< Date: Tue, 28 May 2019 17:42:34 GMT
< Connection: close
< 
* Closing connection 0
[]

```

#### Viewing test and mutation coverage

1. Run the pitest gradle task: `./gradlew clean pitest`
1. Open the html file generated with your browser located at `build/reports/pitest/<--timestamp-->/index.html`

#### Fixing errors reported by Pitest

1. Checkout the fixes with `git checkout v2`
1. Rerun the steps from _"Viewing test and mutation coverage"_ above

#### What changed?

* See the differences with `git diff v1 v2`

---

## License

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**
- Copyright 2019 © <a href="" target="_blank"></a>.