package com.example.mutationtests;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MutationTestsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MutationTestsApplication.class, args);
	}

}
