package com.example.mutationtests.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@RestController
public class RandomUuidController {

    @RequestMapping("/randomuuid/{amount}")
    public ResponseEntity<List> getRandomUuid(@PathVariable("amount") String amount) {
        List<String> uuidList = new LinkedList<>();
        HttpStatus status = HttpStatus.OK;

        try {
            int limit = Integer.parseInt(amount);

            for (int i = 0; i < limit; i++) {
                uuidList.add(UUID.randomUUID().toString());
            }
        } catch (Exception ex) {
            status = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity(uuidList, status);
    }

}
