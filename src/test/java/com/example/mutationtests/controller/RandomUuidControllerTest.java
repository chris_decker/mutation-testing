package com.example.mutationtests.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class RandomUuidControllerTest {

    @InjectMocks
    private RandomUuidController subjectUnderTest;

    @Test
    public void returnsUuids() {
        ResponseEntity<List> expected = subjectUnderTest.getRandomUuid("3");

        assertEquals(3, expected.getBody().size());
    }

    @Test
    public void returnsNothingForBadInput() {
        ResponseEntity<List> expected = subjectUnderTest.getRandomUuid("n");

        assertTrue(expected.getBody().isEmpty());
    }

}
